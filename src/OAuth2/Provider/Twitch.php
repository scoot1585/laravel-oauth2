<?php 

namespace OAuth2\Provider;

use OAuth2\Provider;
use OAuth2\Token_Access;
use OAuth2\Exception;

class Twitch extends Provider {  
	
	public $name = 'twitch';

	public $scope_seperator = ' ';
	
	protected $method = 'POST';

	public function url_authorize()
	{
		return 'https://api.twitch.tv/kraken/oauth2/authorize';
	}

	public function url_access_token()
	{
		return 'https://api.twitch.tv/kraken/oauth2/token';
	}

	public function __construct(array $options = array())	{
		// Now make sure we have the default scope to get user data
		empty($options['scope']) and $options['scope'] = array(
			'user_read' 
		);

		// Array it if its string
		$options['scope'] = (array) $options['scope'];
		
		parent::__construct($options);
	}

	/*
	* Get access to the API
	*
	* @param	string	The access code
	* @return	object	Success or failure along with the response details
	*/	
	public function access($code, $options = array()) {
		if ($code === null) {
			throw new Exception(array('message' => 'Expected Authorization Code from '.ucfirst($this->name).' is missing'));
		}

		return parent::access($code, $options);
	}

	public function get_user_info(Token_Access $token) {
		$url = 'https://api.twitch.tv/kraken/user?'.http_build_query(array(
			'oauth_token' => $token->access_token,
		));
	
		try {	
			$user = json_decode(file_get_contents($url), true);
			return $user;
		} catch (OAuth2_Exception $e) {
			return null;
		} catch (\ErrorException $e) {
			return null;
		}
	}

	public function user_channel_subscription(Token_Access $token, $user, $channel) {
		$url = 'https://api.twitch.tv/kraken/users/' . $user . '/subscriptions/' . $channel . '?'.http_build_query(array(
      'oauth_token' => $token->access_token,
    ));

		try {
			$subscribed = json_decode(file_get_contents($url), true);
			return $subscribed;
		} catch (OAuth2_Exception $e) {
			return null;
		} catch (\ErrorException $e) {
			return null;
		}
	}
}
